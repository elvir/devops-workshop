include make-app.mk
include make-ansible.mk
include make-production.mk

test:
	bin/rails db:test:prepare
	bin/rails test -d

.PHONY: test
